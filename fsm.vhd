-- fsm.vhd: Finite State Machine
-- Author(s): Jan Svabik (xsvabi00)
-- xsvabi00 : kod1 = 1725895078 	 kod2 = 1721790156
--
library ieee;
use ieee.std_logic_1164.all;
-- ----------------------------------------------------------------------------
--                        Entity declaration
-- ----------------------------------------------------------------------------
entity fsm is
port(
   CLK         : in  std_logic;
   RESET       : in  std_logic;

   -- Input signals
   KEY         : in  std_logic_vector(15 downto 0);
   CNT_OF      : in  std_logic;

   -- Output signals
   FSM_CNT_CE  : out std_logic;
   FSM_MX_MEM  : out std_logic;
   FSM_MX_LCD  : out std_logic;
   FSM_LCD_WR  : out std_logic;
   FSM_LCD_CLR : out std_logic
);
end entity fsm;

-- ----------------------------------------------------------------------------
--                      Architecture declaration
-- ----------------------------------------------------------------------------
architecture behavioral of fsm is
   type t_state is (TEST1, TEST2, TEST3, TEST4, TEST5A, TEST5B, TEST6, TEST7, TEST8A, TEST8B, TEST9A, TEST9B, TEST10A, TEST10B, TEST_FINAL, WRONG, PRINT_MESSAGE, PRINT_MESSAGE_OK, FINISH);
   signal present_state, next_state : t_state;

begin
-- -------------------------------------------------------
sync_logic : process(RESET, CLK)
begin
   if (RESET = '1') then
      present_state <= TEST1;
   elsif (CLK'event AND CLK = '1') then
      present_state <= next_state;
   end if;
end process sync_logic;

-- -------------------------------------------------------
next_state_logic : process(present_state, KEY, CNT_OF)
begin
   case (present_state) is
   -- key >1
   when TEST1 =>
      next_state <= TEST1;
      if (KEY(15) = '1') then
         next_state <= PRINT_MESSAGE; 
      elsif (KEY(1) = '1') then
         next_state <= TEST2;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= WRONG;
      end if;
   -- key 1>7
   when TEST2 =>
      next_state <= TEST2;
      if (KEY(15) = '1') then
         next_state <= PRINT_MESSAGE; 
      elsif (KEY(7) = '1') then
         next_state <= TEST3;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= WRONG;
      end if;
   -- key 17>2
   when TEST3 =>
      next_state <= TEST3;
      if (KEY(15) = '1') then
         next_state <= PRINT_MESSAGE; 
      elsif (KEY(2) = '1') then
         next_state <= TEST4;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= WRONG;
      end if;
   -- key 172>5/1
   when TEST4 =>
      next_state <= TEST4;
      if (KEY(15) = '1') then
         next_state <= PRINT_MESSAGE; 
      elsif (KEY(5) = '1') then -- 1725
         next_state <= TEST5A;
      elsif (KEY(1) = '1') then -- 1721
         next_state <= TEST5B;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= WRONG;
      end if;
   -- key 1725>8
   when TEST5A =>
      next_state <= TEST5A;
      if (KEY(15) = '1') then
         next_state <= PRINT_MESSAGE; 
      elsif (KEY(8) = '1') then
         next_state <= TEST6;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= WRONG;
      end if;
   -- key 1721>7
   when TEST5B =>
      next_state <= TEST5B;
      if (KEY(15) = '1') then
         next_state <= PRINT_MESSAGE; 
      elsif (KEY(7) = '1') then
         next_state <= TEST6;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= WRONG;
      end if;
   -- key 17258/17217>9
   when TEST6 =>
      next_state <= TEST6;
      if (KEY(15) = '1') then
         next_state <= PRINT_MESSAGE; 
      elsif (KEY(9) = '1') then
         next_state <= TEST7;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= WRONG;
      end if;
   -- key 172589/172179>5/0
   when TEST7 =>
      next_state <= TEST7;
      if (KEY(15) = '1') then
         next_state <= PRINT_MESSAGE; 
      elsif (KEY(5) = '1') then -- 1725895
         next_state <= TEST8A;
      elsif (KEY(0) = '1') then -- 1721790
         next_state <= TEST8B;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= WRONG;
      end if;
   -- key 1725895>0
   when TEST8A =>
      next_state <= TEST8A;
      if (KEY(15) = '1') then
         next_state <= PRINT_MESSAGE; 
      elsif (KEY(0) = '1') then
         next_state <= TEST9A;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= WRONG;
      end if;
   -- key 1721790>1
   when TEST8B =>
      next_state <= TEST8B;
      if (KEY(15) = '1') then
         next_state <= PRINT_MESSAGE; 
      elsif (KEY(1) = '1') then
         next_state <= TEST9B;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= WRONG;
      end if;
   -- key 17258950>7
   when TEST9A =>
      next_state <= TEST9A;
      if (KEY(15) = '1') then
         next_state <= PRINT_MESSAGE; 
      elsif (KEY(7) = '1') then
         next_state <= TEST10A;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= WRONG;
      end if;
   -- key 17217901>5
   when TEST9B =>
      next_state <= TEST9B;
      if (KEY(15) = '1') then
         next_state <= PRINT_MESSAGE; 
      elsif (KEY(5) = '1') then
         next_state <= TEST10B;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= WRONG;
      end if;
   -- key 172589507>8
   when TEST10A =>
      next_state <= TEST10A;
      if (KEY(15) = '1') then
         next_state <= PRINT_MESSAGE; 
      elsif (KEY(8) = '1') then
         next_state <= TEST_FINAL;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= WRONG;
      end if;
   -- key 172179015>6
   when TEST10B =>
      next_state <= TEST10B;
      if (KEY(15) = '1') then
         next_state <= PRINT_MESSAGE; 
      elsif (KEY(6) = '1') then
         next_state <= TEST_FINAL;
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= WRONG;
      end if;
   -- confirm or wrong
   when TEST_FINAL =>
      next_state <= TEST_FINAL;
      if (KEY(15) = '1') then
         next_state <= PRINT_MESSAGE_OK; 
      elsif (KEY(14 downto 0) /= "000000000000000") then
         next_state <= WRONG;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when WRONG =>
      next_state <= WRONG;
      if (KEY(15) = '1') then
         next_state <= PRINT_MESSAGE; 
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when PRINT_MESSAGE =>
      next_state <= PRINT_MESSAGE;
      if (CNT_OF = '1') then
         next_state <= FINISH;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when PRINT_MESSAGE_OK =>
      next_state <= PRINT_MESSAGE_OK;
      if (CNT_OF = '1') then
         next_state <= FINISH;
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when FINISH =>
      next_state <= FINISH;
      if (KEY(15) = '1') then
         next_state <= TEST1; 
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when others =>
      next_state <= TEST1;
   end case;
end process next_state_logic;

-- -------------------------------------------------------
output_logic : process(present_state, KEY)
begin
   FSM_CNT_CE     <= '0';
   FSM_MX_MEM     <= '0';
   FSM_MX_LCD     <= '0';
   FSM_LCD_WR     <= '0';
   FSM_LCD_CLR    <= '0';

   case (present_state) is
   -- - - - - - - - - - - - - - - - - - - - - - -
   when TEST1 =>
      if (KEY(14 downto 0) /= "000000000000000") then
         FSM_LCD_WR     <= '1';
      end if;
      if (KEY(15) = '1') then
         FSM_LCD_CLR    <= '1';
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when PRINT_MESSAGE =>
      FSM_CNT_CE     <= '1';
      FSM_MX_LCD     <= '1';
      FSM_LCD_WR     <= '1';
   -- - - - - - - - - - - - - - - - - - - - - - -
   when PRINT_MESSAGE_OK =>
      FSM_MX_MEM     <= '1';
      FSM_CNT_CE     <= '1';
      FSM_MX_LCD     <= '1';
      FSM_LCD_WR     <= '1';
   -- - - - - - - - - - - - - - - - - - - - - - -
   when FINISH =>
      if (KEY(15) = '1') then
         FSM_LCD_CLR    <= '1';
      end if;
   -- - - - - - - - - - - - - - - - - - - - - - -
   when others =>
	   if (KEY(14 downto 0) /= "000000000000000") then
         FSM_LCD_WR     <= '1';
      end if;
      if (KEY(15) = '1') then
         FSM_LCD_CLR    <= '1';
      end if;
   end case;
end process output_logic;

end architecture behavioral;

